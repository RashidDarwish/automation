package org.lean.testing;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ComputationTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ComputationTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( ComputationTest.class );
    }

    public void testisEqual(){
         assertFalse(new Computation().isEqual(1, 2));
    }

    public void testisEqual1(){
       assertTrue(new Computation().isEqual(2, 2));
    }
}
