package cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "html:target/cucumber", "json:target/json/output.json"}, features = {
        "classpath:features/"},glue = {"def" })
public class RunCukesTest {
}//http://JenkinsAdmin:20061644d9d3ba1ed04c748fb1c35985@83.248.50.110// Working