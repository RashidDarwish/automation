package def;

import cucumber.api.PendingException;
import cucumber.api.java8.En;

public class MyStepDefs implements En {

    public MyStepDefs() {
        Given("^two numbers$", () -> {
            String str = "Given";
            System.out.format("Step: %s\n", str);
        });

        When("^numbers are equals$", () -> {
            String str = "When";
            System.out.format("Step: %s\n", str);
        });

        Then("^result is true$", () -> {
            String str = "Then";
            System.out.format("Step: %s\n", str);
        });
        And("^use (\\d+) minus (\\d+)$", (Integer arg0, Integer arg1) -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
        And("^check \"([^\"]*)\" lengeh$", (String arg0) -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });


    }
}
